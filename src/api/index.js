import axios from 'axios'

const API = axios.create({
  baseURL: `http://localhost:8000`
})

const getUser = () => {
  const accessToken = localStorage.getItem('access-token')
  return API.get('/', {
    headers: {
      "access-token": accessToken
    }
  })
}

const getPlaylists = () => {
  const accessToken = localStorage.getItem('access-token')
  return API.get('/playlists', {
    headers: {
      "access-token": accessToken
    }
  })
}

const getFeaturedArtists = () => {
  const accessToken = localStorage.getItem('access-token')
  return API.get('/featured-artists', {
    headers: {
      "access-token": accessToken
    }
  })
}

export {
  getUser,
  getPlaylists,
  getFeaturedArtists
}