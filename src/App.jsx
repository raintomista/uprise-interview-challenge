import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import { Router, Redirect } from '@reach/router'
import Overview from 'views/Overview'
import Login from 'views/Login'
import AuthSuccess from 'views/AuthSuccess'
import FeaturedArtists from 'views/FeaturedArtists'
import Playlists from 'views/Playlists'

import 'styles/reset.css'
import 'styles/global.css'

const App = () => {
  return (
    <Router>
      <Overview path="/"/>
      <Login path="/login"/>
      <AuthSuccess path="/authenticated"/>
      <FeaturedArtists path="/featured"/>
      <Playlists path="/playlists"/>
    </Router>
  )
}

ReactDOM.render(<App/>, document.getElementById('root'))