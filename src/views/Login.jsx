import React from 'react'
import Layout from 'components/Layout'
import { Button } from 'components/Login'

const Login = () => {
  const handleClick = () => {
    const loginUrl = `https://accounts.spotify.com/authorize?client_id=ce282a4b49cf4c70b3a4645e381f7999&response_type=token&redirect_uri=http://localhost:1234/authenticated`
    window.open(loginUrl, 'Spotify', 'width=500, height=700')
  }

  return (
    <Layout>
      <Button onClick={handleClick} title="Login with Spotify"/>
    </Layout>
  )
}

export default Login