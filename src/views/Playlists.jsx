import React, { useEffect, useState } from 'react'
import { useNavigate } from '@reach/router'

import Layout from 'components/Layout'
import { Content, Loader } from 'components/Playlists'
import * as API from 'api'

const Playlists = () => {
  const navigate = useNavigate()
  const [playlists, setPlaylists] = useState(null)
  const [currIndex, setCurrIndex] = useState(null)

  const getPlaylists = async () => {
    try {
      if (!localStorage.getItem('access-token')) {
        throw new Error()
      }
      const response = await API.getPlaylists()
      setPlaylists(response.data.user.playlists)
      setCurrIndex(0)
    } catch (err) {
      alert('Unauthorized access. Please login with your Spotify account.')
      navigate('/login')
    }
  }
  
  const handleNext = () => {
    currIndex + 1 >= playlists.length
      ? setCurrIndex(0)
      : setCurrIndex(currIndex + 1)
  }

  const handlePrev = () => {
    currIndex - 1 < 0
      ? setCurrIndex(playlists.length - 1)
      : setCurrIndex(currIndex - 1)
  }

  useEffect(() => {
    getPlaylists()
  }, [])
  
  return (
    <Layout>
      {!(playlists && playlists[currIndex])
        ? <Loader/>
        : (
          <Content
            handlePrev={handlePrev}
            handleNext={handleNext}
            playlist={playlists[currIndex]}
          />
        )
      }
    </Layout>
  )
}

export default Playlists