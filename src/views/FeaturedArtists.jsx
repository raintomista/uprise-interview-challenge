import React, { useEffect, useState } from 'react'
import { useNavigate } from '@reach/router'
import { Card as BaseCard } from '@uprise/card'
import { backgrounds } from '@uprise/colors'
import styled from 'styled-components'

import { Button } from 'components/FeaturedArtists'
import Layout from 'components/Layout'
import * as API from 'api'

const Card = styled(BaseCard)`
  border-radius: 5px;
  position: relative;
  top: -31px;
  left: 214.04px;
`

const FeaturedArtists = () => {
  const navigate = useNavigate()
  const [featured, setFeatured] = useState([])

  const getFeatured = async () => {
    try {
      const response = await API.getFeaturedArtists()
      setFeatured(response.data.artists)
    } catch(err) {
      alert('Unauthorized access. Please login with your Spotify account.')
      navigate('/login', {
        replace: true
      })
    }
  }

  useEffect(() => {
    getFeatured()
  }, [])

  return (
    <Layout>
      <Card
        backgroundColor={backgrounds.white}
        shadow={true}
        width="250px"
      >
        {featured.map((artist, index) => (
          <Button
            key={index}
            title={artist}
            size="medium"
          />  
        ))}
      </Card>
    </Layout>
  )
}

export default FeaturedArtists