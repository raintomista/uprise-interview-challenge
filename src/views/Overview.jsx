import React, { useEffect, useState } from 'react'
import { useNavigate } from '@reach/router'
import { Loader as BaseLoader } from '@uprise/loader'
import styled from 'styled-components'

import { Content } from 'components/Overview'
import Layout from 'components/Layout'
import * as API from 'api'


const Loader = styled(BaseLoader)`
  position: static;
  margin-top: 31px;
  height: 50px;
  width: 50px;
`

const Overview = props => {
  const navigate = useNavigate()
  const [user, setUser] = useState(null)

  const getUserInfo = async () => {
    try {
      if (!localStorage.getItem('access-token')) {
        throw new Error()
      }
      
      const response = await API.getUser()
      setUser(response.data.user)
    } catch (err) {
      alert('Unauthorized access. Please login with your Spotify account.')
      navigate('/login')
    }
  }

  useEffect(() => {
    getUserInfo()
  }, [])

  return (
    <Layout>
      {!user
        ? <Loader/>
        : <Content user={user}/>
      }
    </Layout>
  )
}

export default Overview