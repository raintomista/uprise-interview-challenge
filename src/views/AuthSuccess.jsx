import React, { useEffect } from 'react'
import { useLocation, Redirect, useNavigate } from "@reach/router"
import { parse } from 'query-string'
import Layout from '../components/Layout'


const AuthSuccess = props => {
  const navigate = useNavigate()
  const location = useLocation()
  const hash = parse(location.hash.substr(1))

  useEffect(() => {
    if (hash) {
      localStorage.setItem('access-token', hash.access_token)
      window.opener.location = 'http://localhost:1234/'
      window.close()
    }
  }, [])

  return (
    <Layout>
      Redirecting to Uprise App
    </Layout>
  )
}

export default AuthSuccess