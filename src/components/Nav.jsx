import React from 'react'
import styled from 'styled-components'
import { useLocation, useNavigate } from '@reach/router'

import { Button as BaseButton } from '@uprise/button'
import { Card as BaseCard } from '@uprise/card'
import { backgrounds, extended, primary } from '@uprise/colors'

const Card = styled(BaseCard)`
  display: flex;
  margin-bottom: 31px;
`

const Button = styled(BaseButton)`
  color: ${extended.charcoal.one};
  font-family: 'Montserrat', sans-serif;
  font-weight: 400;
  font-size: 15px;
  height: 20px;
  line-height: 20px;
  padding: 0px 32px 0px 0px;
  &.active {
    color: ${primary.purple};
    font-weight: 500;
  }
`

const Nav = () => {
  const navigate = useNavigate()
  const location = useLocation()

  
  return (
    <Card
      backgroundColor={backgrounds.white}
      shadow={true}
      padding="25px 40px"
    >
      <Button
        className={location.pathname === '/' ? 'active' : ''}
        fullWidth={false}
        title="Overview"
        variant="text"
        onClick={() => navigate('/', { replace: true })}
      />
      <Button
        className={location.pathname === '/playlists' ? 'active' : ''}
        fullWidth={false}
        title="Playlist"
        variant="text"
        onClick={() => navigate('/playlists', { replace: true })}
      />
      <Button
        className={location.pathname === '/featured' ? 'active' : ''}
        fullWidth={false}
        title="Featured"
        variant="text"
        onClick={() => navigate('/featured', { replace: true })}
      />
    </Card>
  )
}

export default Nav