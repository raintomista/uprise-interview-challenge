import React from 'react'
import styled from 'styled-components'
import { Button as BaseButton } from '@uprise/button'

export const Button = styled(BaseButton)`
  font-family: 'Montserrat', sans-serif;
  font-size: 14px;
  font-weight: 400;
  height: 42px;
  width: 200px;
`