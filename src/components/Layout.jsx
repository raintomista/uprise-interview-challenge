import React from 'react'
import styled from 'styled-components'
import { Col, Container, Row } from '@uprise/grid'
import { backgrounds } from '@uprise/colors'

import Nav from './Nav'

const Background = styled.div`
  background-color: ${backgrounds.fadedPurple};
  height: 100vh;
`

const Layout = props => (
  <Background>
    <Container>
      <Row>
        <Col>
          <Nav/>
        </Col>
      </Row>
      <Row>
        <Col>
          {props.children}
        </Col>
      </Row>
    </Container>
  </Background>
)

export default Layout