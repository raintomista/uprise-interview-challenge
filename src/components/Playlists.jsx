import React from 'react'
import styled from 'styled-components'

import { backgrounds, primary } from '@uprise/colors'
import { Loader as BaseLoader } from '@uprise/loader'
import { Card as BaseCard } from '@uprise/card'
import { H2 } from '@uprise/headings'
import Icons from '@uprise/icons'

const Card = styled(BaseCard)`
  display: flex;
  justify-content: center;
`

const PrevButton = styled.button`
  background-color: transparent;
  border: 1px solid ${primary.purple};
  border-radius: 100%;
  margin: 95px 40px 95px 0px;
  height: 50px;
  width: 50px;
`

const PrevIcon = styled.div`
  transform: rotate(90deg);
  user-select: none;
`

const NextButton = styled.button`
  background-color: transparent;
  border: 1px solid ${primary.purple};
  border-radius: 100%;
  margin: 95px 0px 95px 40px;
  height: 50px;
  width: 50px;
`

const NextIcon = styled.div`
  transform: rotate(-90deg);
  user-select: none;
`

const Image = styled.div`
  background-color: ${backgrounds.fadedPurple};
  border: none;
  border-radius: 10px;
  display: block;
  height: 240px;
  width: 320px;
  &> img {
    object-fit: scale-down;
    height: 100%;
    width: 100%;
  }
`

const Title = styled(H2)`
  color: ${primary.charcoal};
  font-family: 'Montserrat', sans-serif;
  font-weight: 600;
  font-size: 24px;
  line-height: 32px;
  margin-top: 24px;
  width: 320px;
  text-align: center;
`

const Playlist = styled.div`
  display: block;
`

export const Loader = styled(BaseLoader)`
  position: static;
  margin-top: 31px;
  height: 50px;
  width: 50px;
`

export const Content = ({ playlist, ...props }) => (
  <Card
    backgroundColor={backgrounds.white}
    padding="40px"
    shadow={true}
  >
    <PrevButton onClick={props.handlePrev}>
      <PrevIcon>
        <img src={Icons.chevronPurple}/>
      </PrevIcon>
    </PrevButton>
    <Playlist>
      <Image>
        <img src={playlist.images[0].url}/>
      </Image>
      <Title>
        {playlist.name}
      </Title>
    </Playlist>
    <NextButton onClick={props.handleNext}>
      <NextIcon>
        <img src={Icons.chevronPurple}/>
      </NextIcon>
    </NextButton>
  </Card>
)