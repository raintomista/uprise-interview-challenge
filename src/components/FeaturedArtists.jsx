import React from 'react'
import styled from 'styled-components'
import { Button as BaseButton } from '@uprise/button'
import { backgrounds, extended, primary } from '@uprise/colors'

export const Button = styled(BaseButton)`
  background-color: transparent;
  border: none;
  border-radius: 0px;
  color: ${extended.charcoal.one};
  font-family: 'Montserrat', sans-serif;
  font-weight: 400;
  font-size: 15px;
  height: 40px;
  justify-content: flex-start;
  padding: 0px 20px;
  &:hover {
    color: ${backgrounds.white};
  }
`