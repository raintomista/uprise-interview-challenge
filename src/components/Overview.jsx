import React from 'react'
import styled from 'styled-components'

import { backgrounds, primary, extended } from '@uprise/colors'
import { H1, H3 } from '@uprise/headings'
import { Button as BaseButton } from '@uprise/button'
import { Card as BaseCard } from '@uprise/card'

const Card = styled(BaseCard)`
  display: grid;
  grid-template-columns: 0.4fr 0.6fr;
`

const Image = props => {
  const imageContainer = `
    background-color: ${backgrounds.fadedPurple};
    border: none;
    border-radius: 10px;
    height: 240px;
    width: 320px;
  `

  const image = `
    object-fit: scale-down;
    height: 100%;
    width: 100%;
  `

  return (
    <div css={imageContainer}>
      <img
        css={image}
        src={props.src}
      />
    </div>
  )
}

const Title = styled(H1)`
  color: ${primary.charcoal};
  font-family: 'Montserrat', sans-serif;
  font-weight: 600;
  font-size: 32px;
  line-height: 40px;
`

const SubTitle = styled(H3)`
  color: ${extended.charcoal.one};
  font-family: 'Montserrat', sans-serif;
  font-weight: 400;
  font-size: 18px;
  line-height: 28px;
`

const Button = styled(BaseButton)`
  font-family: 'Montserrat', sans-serif;
  font-size: 14px;
  font-weight: 400;
  height: 42px;
  width: 120px;
`

const Details = props => {
  const detailsContainer = `
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `

  return (
    <div css={detailsContainer}>
      <div>
        <Title>
          {props.user.display_name}
        </Title>
        <SubTitle>
          Followers (406,013)
        </SubTitle>
      </div>
      <Button
        title="Follow"
        fullWidth={false}
        padding
      />
    </div>
  )
}

export const Content = props => (
  <Card
    backgroundColor={backgrounds.white}
    padding="45px"
    shadow={true}
  >
    <Image src={props.user.images[0].url}/>
    <Details user={props.user}/>
  </Card>
)