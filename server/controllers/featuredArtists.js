const spotify_graphql = require('spotify-graphql')
const { shuffle } = require('lodash')

exports.getFeaturedArtists = async (req, res) => {
  const config = {
    clientId: process.env.SPOTIFY_CLIENT_ID,
    clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
    redirectUri: process.env.SPOTIFY_REDIRECT_URI,
    accessToken: req.headers['access-token']
  }
  
  spotify_graphql.SpotifyGraphQLClient(config).query(`
    {
      user(id:"chilledcow") {
        display_name,
         playlists {
          name,
          tracks {
            track {
              id,
              artists {
                id,
                name
              }
            }
          }
         }
      }
    }
  `).then(results => {
      if(!results.errors) {
        const playlists = results.data.user.playlists

        const artists = new Set(playlists.flatMap(playlist => {
          return playlist.tracks.flatMap(trackItem => {
            return trackItem.track.artists.map(artist => artist.name)
          })
        }))

        res.status(200).send({
          "artists": shuffle(Array.from(artists)).slice(0, 10)
        })
      } else {
        throw new Error()
      }
  }).catch(() => {
    res.status(401).send({
      errorMsg: 'Unauthorized'
    })
  })

}