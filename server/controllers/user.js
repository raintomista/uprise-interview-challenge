const spotify_graphql = require('spotify-graphql')

exports.getUser = async (req, res) => {
  const config = {
    clientId: process.env.SPOTIFY_CLIENT_ID,
    clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
    redirectUri: process.env.SPOTIFY_REDIRECT_URI,
    accessToken: req.headers['access-token']
  }

  spotify_graphql.SpotifyGraphQLClient(config).query(`
    {
      user(id:"chilledcow") {
        display_name,
        images {
          height
          url
          width
        }
      }
    }
  `).then(results => {
    if(!results.errors) {
      res.status(200).send(results.data)
    } else {
      throw new Error()
    }
  }).catch(() => {
    res.status(401).send({
      errorMsg: 'Unauthorized'
    })
  })
}