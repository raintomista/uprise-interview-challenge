const express = require('express')
const request = require('request')
const cors = require('cors')
const querystring = require('querystring')
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv')

const { getUser } = require('./controllers/user')
const { getPlaylists } = require('./controllers/playlist')
const { getFeaturedArtists } = require('./controllers/featuredArtists')

dotenv.config()

const app = express()


app.use(cors())
   .use(cookieParser())

app.get('/login', (req, res) => {
  res.redirect('https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: process.env.SPOTIFY_CLIENT_ID,
      redirect_uri: process.env.SPOTIFY_REDIRECT_URI
    }));
})

app.get('/', getUser)
app.get('/playlists', getPlaylists)
app.get('/featured-artists', getFeaturedArtists)
app.listen(8000)